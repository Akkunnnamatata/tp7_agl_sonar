Sonar lint ne marche que avec du code herbergé en ligne tel que github.com, gitlab.com, ou bitbucket.com ( le domaine du gitlab de la fac étant pas en ligne (disponible)
pour tous, il faut pousser le code autre part.



Pour utiliser SonarCloud :
- utiliser un dépot sur un VCS.com
- creer son compte sonarcloud
- autoriser le compte lié à être utiliser par sonarCloud
- Creer une organisation dans SonarCloud qui est relié au compte que vous venez de lier
  - donner les droits à sonarCloud d'utiliser votre compte VCS.com > générer un token avec les droits api (donne tout les droit necessaire pour l'utilisation d'une api)
  - Choisir le depot à analyser
  - choisir la méthode d'analyse (MAVEN/JS/ ..) et ajouter dans le code sources les modifications à faire (ex .pom)

Pour avoir la couverture de code réalisé par les test, il faut spécifié à Sonar où les prendre.
En rajoutant Jacoco et spécifiant : `-Dsonar.coverage.jacoco.xmlReportPaths=target/site/jacoco/jacoco.xml`
dans la commande d'analyse, il va chercher les résultats correspondant.

Vous noterez que la couverture indiquée par sonar n'est pas la même que celle calculée par Jacoco, Celà est du à la façon dont Sonar calcule la couverture à partir des
données fournies par Jacoco, en faisant une combinaison de couverture de branches et de
couverture d'instructions. 

SonarLint -> extension directement dans IDE pour mettre en valeur certaines erreurs

Pour décider quel fichier analyser par SonarLint mettre `: xml
<sonar.sources>src/main/java,src/main/resources</sonar.sources>` dans le .pom (ici on a donc analysé aussi fichier HTML)